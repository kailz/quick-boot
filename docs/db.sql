/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : quick-boot

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 29/04/2024 13:44:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_info`;
CREATE TABLE `tb_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `sort` int(1) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_info
-- ----------------------------
INSERT INTO `tb_info` VALUES (1, 'Quick Boot', 'Quick Boot系统简介', '<p data-v-106c86ed=\"\">框架官网:&nbsp;<a href=\"https://gitee.com/kailz/quick-boot\" target=\"_blank\" rel=\"noopener\" data-v-106c86ed=\"\">https://gitee.com/kailz/quick-boot</a></p>\n<p data-v-106c86ed=\"\">SpringBoot是一个基于Spring框架的快速开发框架，使开发者可以更加方便快捷地构建Spring应用程序。Shiro是一个强大且易于使用的Java安全框架，提供认证、授权、加密和会话管理等功能。MyBatis是一个优秀的持久化框架，它将Java对象映射到数据库表中，并提供了灵活的查询语言以及强大的SQL执行能力。Redis是一种高性能的内存键值存储系统，可用于缓存、消息队列、分布式锁等多种场景。</p>\n<p data-v-106c86ed=\"\">在SpringBoot中集成Shiro和Mybatis可以实现对用户身份认证和权限控制的支持，同时结合Redis作为缓存存储可以提高系统的响应速度和并发能力。Shiro提供了多种认证方式，如基于身份验证令牌的用户名/密码登录、单点登录（SSO）、OAuth以及CAS等。Mybatis则开发者可以通过注解或XML配置方式定义SQL语句，Mybatis会将这些语句转换成对应的JDBC操作。而SpringBoot对于各种技术的集成和自动化配置都提供了很好的支持，使得开发者能够快速搭建起一套完整的Web应用程序。</p>', 9);

-- ----------------------------
-- Table structure for tb_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_log`;
CREATE TABLE `tb_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '日志时间',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 637 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_log
-- ----------------------------

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单地址',
  `parent` int(255) NULL DEFAULT NULL COMMENT '父级菜单',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES (1, '首页', '/dashboard', 0, 'el-icon-menu');
INSERT INTO `tb_menu` VALUES (2, '系统管理', '/system', 0, 'el-icon-menu');
INSERT INTO `tb_menu` VALUES (3, '用户管理', '/system/user', 0, 'el-icon-menu');
INSERT INTO `tb_menu` VALUES (4, '权限管理', '/system/permission', 2, '');
INSERT INTO `tb_menu` VALUES (5, '角色管理', '/system/role', 2, '');
INSERT INTO `tb_menu` VALUES (6, '菜单管理', '/system/menu', 2, '');
INSERT INTO `tb_menu` VALUES (7, '系统日志', '/system/logs', 2, '');
INSERT INTO `tb_menu` VALUES (8, '公告管理', '/system/info', 2, '');

-- ----------------------------
-- Table structure for tb_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_permission`;
CREATE TABLE `tb_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限字符',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_permission
-- ----------------------------
INSERT INTO `tb_permission` VALUES (1, 'ss-login', '管理员登录');
INSERT INTO `tb_permission` VALUES (2, 'ss-logout', '管理员注销');
INSERT INTO `tb_permission` VALUES (3, 'ss-info', '管理员信息');
INSERT INTO `tb_permission` VALUES (4, 'ss-permission-save', '权限保存');
INSERT INTO `tb_permission` VALUES (5, 'ss-permission-delete', '权限删除');
INSERT INTO `tb_permission` VALUES (6, 'ss-permission-list', '权限列表');
INSERT INTO `tb_permission` VALUES (7, 'ss-role-save', '角色保存');
INSERT INTO `tb_permission` VALUES (8, 'ss-role-list', '角色列表');
INSERT INTO `tb_permission` VALUES (9, 'ss-role-delete', '角色删除');
INSERT INTO `tb_permission` VALUES (10, 'ss-role-allot', '角色分配权限');
INSERT INTO `tb_permission` VALUES (11, 'ss-user-allot', '用户分配角色');
INSERT INTO `tb_permission` VALUES (12, 'ss-user-save', '用户保存');
INSERT INTO `tb_permission` VALUES (13, 'ss-user-delete', '用户删除');
INSERT INTO `tb_permission` VALUES (14, 'ss-user-offline', '用户下线');
INSERT INTO `tb_permission` VALUES (15, 'ss-user-list', '用户列表');
INSERT INTO `tb_permission` VALUES (16, 'ss-log-list', '查看日志');
INSERT INTO `tb_permission` VALUES (17, 'ss-log-delete', '删除日志权限');
INSERT INTO `tb_permission` VALUES (18, 'ss-menu-list', '查询菜单信息');
INSERT INTO `tb_permission` VALUES (19, 'ss-menu-save', '保存菜单信息');
INSERT INTO `tb_permission` VALUES (20, 'ss-menu-delete', '删除菜单信息');
INSERT INTO `tb_permission` VALUES (21, 'ss-menu-allot', '分配角色菜单');
INSERT INTO `tb_permission` VALUES (22, 'ss-info-save', '保存系统信息');
INSERT INTO `tb_permission` VALUES (23, 'ss-info-list', '查看系统信息');
INSERT INTO `tb_permission` VALUES (24, 'ss-info-delete', '删除系统信息');

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, 'admin', '超级管理员');

-- ----------------------------
-- Table structure for tb_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_menu`;
CREATE TABLE `tb_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_menu
-- ----------------------------
INSERT INTO `tb_role_menu` VALUES (34, 1, 1);
INSERT INTO `tb_role_menu` VALUES (35, 1, 2);
INSERT INTO `tb_role_menu` VALUES (36, 1, 3);
INSERT INTO `tb_role_menu` VALUES (37, 1, 4);
INSERT INTO `tb_role_menu` VALUES (38, 1, 5);
INSERT INTO `tb_role_menu` VALUES (39, 1, 6);
INSERT INTO `tb_role_menu` VALUES (40, 1, 7);
INSERT INTO `tb_role_menu` VALUES (41, 1, 8);

-- ----------------------------
-- Table structure for tb_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_permission`;
CREATE TABLE `tb_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色ID',
  `permission_id` int(11) NULL DEFAULT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 180 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_role_permission
-- ----------------------------
INSERT INTO `tb_role_permission` VALUES (81, 2, 1);
INSERT INTO `tb_role_permission` VALUES (82, 2, 2);
INSERT INTO `tb_role_permission` VALUES (83, 2, 3);
INSERT INTO `tb_role_permission` VALUES (84, 2, 4);
INSERT INTO `tb_role_permission` VALUES (85, 2, 5);
INSERT INTO `tb_role_permission` VALUES (86, 2, 6);
INSERT INTO `tb_role_permission` VALUES (87, 2, 7);
INSERT INTO `tb_role_permission` VALUES (88, 2, 8);
INSERT INTO `tb_role_permission` VALUES (89, 2, 9);
INSERT INTO `tb_role_permission` VALUES (90, 2, 10);
INSERT INTO `tb_role_permission` VALUES (91, 2, 11);
INSERT INTO `tb_role_permission` VALUES (92, 2, 12);
INSERT INTO `tb_role_permission` VALUES (93, 2, 13);
INSERT INTO `tb_role_permission` VALUES (94, 2, 14);
INSERT INTO `tb_role_permission` VALUES (95, 2, 15);
INSERT INTO `tb_role_permission` VALUES (96, 2, 17);
INSERT INTO `tb_role_permission` VALUES (97, 2, 16);
INSERT INTO `tb_role_permission` VALUES (156, 1, 1);
INSERT INTO `tb_role_permission` VALUES (157, 1, 2);
INSERT INTO `tb_role_permission` VALUES (158, 1, 3);
INSERT INTO `tb_role_permission` VALUES (159, 1, 4);
INSERT INTO `tb_role_permission` VALUES (160, 1, 5);
INSERT INTO `tb_role_permission` VALUES (161, 1, 6);
INSERT INTO `tb_role_permission` VALUES (162, 1, 7);
INSERT INTO `tb_role_permission` VALUES (163, 1, 8);
INSERT INTO `tb_role_permission` VALUES (164, 1, 9);
INSERT INTO `tb_role_permission` VALUES (165, 1, 10);
INSERT INTO `tb_role_permission` VALUES (166, 1, 11);
INSERT INTO `tb_role_permission` VALUES (167, 1, 12);
INSERT INTO `tb_role_permission` VALUES (168, 1, 13);
INSERT INTO `tb_role_permission` VALUES (169, 1, 14);
INSERT INTO `tb_role_permission` VALUES (170, 1, 15);
INSERT INTO `tb_role_permission` VALUES (171, 1, 16);
INSERT INTO `tb_role_permission` VALUES (172, 1, 17);
INSERT INTO `tb_role_permission` VALUES (173, 1, 18);
INSERT INTO `tb_role_permission` VALUES (174, 1, 19);
INSERT INTO `tb_role_permission` VALUES (175, 1, 20);
INSERT INTO `tb_role_permission` VALUES (176, 1, 21);
INSERT INTO `tb_role_permission` VALUES (177, 1, 23);
INSERT INTO `tb_role_permission` VALUES (178, 1, 22);
INSERT INTO `tb_role_permission` VALUES (179, 1, 24);

-- ----------------------------
-- Table structure for tb_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_token`;
CREATE TABLE `tb_token`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'jwt token',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_token
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `disabled` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'admin', 'admin', '超级管理员', 'http://localhost:9090/files/get?filename=0319a9b4-f7e6-4923-bf99-8b32d550e4ea.jpg', '13531196495', 0);
INSERT INTO `tb_user` VALUES (3, 'user', '123456', '林凯', 'http://localhost:9090/files/get?filename=44882aaa-9b6d-4433-941b-9495c445c07e.jpg', '13531196495', 0);

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (1, 1, 1);
INSERT INTO `tb_user_role` VALUES (2, 2, 2);

SET FOREIGN_KEY_CHECKS = 1;
