# 个人信息

开发者微信：a13531196495，如有问题可以联系。

# 项目结构

## ADMIN目录:

此目录存放后台管理系统的前端文件，使用Vue+Element+Axios

### 特性：

| 功能名称 | 功能使用                                             | 功能描述                                |
| -------- | ---------------------------------------------------- | --------------------------------------- |
| 角色校验 | 使用标签属性v-has-roles,传入数组字符串               | 如果登录用户没有此角色隐藏              |
| 权限校验 | 使用标签属性v-has-permissions,传入数组字符串         | 如果登录用户没有此权限隐藏              |
| 路由新规 | 无需修改router/index.js，只需要操作dynamic-routes.js | 使用buildRoute和buildRoutes进行创建路由 |

### 路由新规：

引入工具包依赖：

```
import { buildRoutes, buildRoute } from '@/utils/routes'
```

创建主路由和子路由：

```
const systemRoutes = buildRoute('/system', [
  buildRoutes('用户管理', '/system/user', () => import('@/views/system/user')),
  buildRoutes('权限管理', '/system/permission', () => import('@/views/system/permission')),
  buildRoutes('角色管理', '/system/role', () => import('@/views/system/role')),
  buildRoutes('菜单管理', '/system/menu', () => import('@/views/system/menu')),
  buildRoutes('日志管理', '/system/logs', () => import('@/views/system/logs')),
  buildRoutes('修改个人信息', '/system/profile', () => import('@/views/system/profile'))
])
```

导出路由数组：

```
export default [
  systemRoutes
]
```

### 角色校验：

通过v-has-roles="[]"进行校验角色，如果角色不存在则不显示元素。

示例：只有角色拥有admin的话才可以显示

```
<Particles v-has-roles="['admin']"/>
```

示例：只有角色拥有admin或者user其中一个才可以显示

```
<Particles v-has-roles="['admin','user']"/>
```

示例：方法内操作

```
if (this.checkAnyRoles('admin', 'user')) {
// 校验角色通过admin或者user其中一个
}
```

### 权限校验：

通过v-has-permissions="[]"进行校验权限，如果权限不存在则不显示元素。

示例：只有角色拥有ss-user-save权限的话才可以显示

```
<Particles v-has-permissions="['ss-user-save']"/>
```

示例：只有角色拥有ss-user-save或ss-user-edit权限的话才可以显示

```
<Particles v-has-permissions="['ss-user-save','ss-user-edit']"/>
```

示例：方法内操作

```
if (this.checkAnyPermissions('ss-user-save','ss-user-edit')) {
// 只有角色拥有ss-user-save或ss-user-edit权限的话才通过
}
```

## API目录：

### 特性：

| 功能名称        | 功能使用                                                     | 功能描述                                                  |
| --------------- | ------------------------------------------------------------ | --------------------------------------------------------- |
| 获取登录用户名  | LoginHelper.getUsername();                                   | 获取当前登录的用户名                                      |
| 集成JWT认证     | 0配置直接使用                                                | 集成JWT认证，使用Shiro+JWT                                |
| 集成MybatisPlus | 0配置直接使用                                                | 集成MybatisPlus                                           |
| 分布式Token     | 在JWTConfig中的tokenDAO方法，返回MysqlTokenDAO或者是RedisTokenDAO，内置了MysqlTokenDAO和RedisTokenDAO，可继承TokenDAO类自行拓展 | 为了实现分布式部署，直接用户登录信息共享，达到SSO单点登录 |

## 功能简介：

### 用户管理

#### 查看用户列表：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/用户管理.png)

#### 强制下线：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/强制下线.png)

#### 分配用户角色：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/分配用户角色.png)

### 权限管理：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/权限管理.png)

### 菜单管理：

#### 菜单列表：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/菜单管理.png)

#### 多级菜单：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/多级菜单.png)

### 日志查看：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/日志查看.png)

### 角色管理：

#### 查看角色信息：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/角色列表.png)

#### 分配角色权限：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/分配角色权限.png)

#### 分配角色路由：

![](https://gitee.com/kailz/quick-boot/raw/master/docs/分配角色菜单.png)

## 项目打包：

### 前端：

```
安装依赖: 
npm install //如果卡顿请用淘宝源
npm run dev //测试环境启动
npm rum build:prod // 生产环境打包

// 生产环境打包注意事项：
修改.env.production，配置VUE_APP_BASE_API=你的域名地址
// 静态资源问题
配置vue.config.js的publicPath，默认为/，可配置成需要的地址
// 配置打包路径
配置vue.config.js的outputDir，默认在admin/dist中

// 关于设置淘宝源的命令
npm config set registry https://registry.npm.taobao.org/
```



### 后端：

```
// 开发环境
配置application-dev.yml
// 生产环境
将application.yml的spring.profile.active修改为prod
// 清除缓存
在quick-boot根目录执行mvn clean清空缓存信息
// 打包
mvn clean package -DskipTests
打包成功后会在目录admin/target/中
```



### application.yml的自定义配置

```
jwt.secret // 密钥，jwt生成的密码
jwt.client // 客户Id，客户端的id，不需要做中台验证的话，选填
jwt.provide_open // 是否打开登录登出校验secret和client，不需要做中台验证的话，直接为false
upload.savePath // 上传功能保存的路径
upload.httpPath // 上传功能上传成功后访问资源的地址，默认为http://ip:port/files/get
```

